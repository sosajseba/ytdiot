﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Interfaces;

namespace YTDIoT.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenericController<T> : ControllerBase where T : BaseEntity
    {
        private readonly IGenericService<T> _genericService;

        public GenericController(IGenericService<T> genericService)
        {
            _genericService = genericService;
        }

        // GET api/<GenericController>/5
        [HttpGet("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<T>> Get(int id)
        {
            try
            {
                return Ok(await _genericService.Get(id));
            }
            catch (Exception)
            {
                return BadRequest(_genericService.Error);
            }
        }

        // POST api/<GenericController>
        [HttpPost]
        public async Task<ActionResult<T>> Post([FromBody] T value)
        {
            try
            {
                return Ok(await _genericService.Create(value));
            }
            catch (Exception)
            {
                return BadRequest(_genericService.Error);
            }
        }

        // PUT api/<GenericController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult<T>> Put(int id, [FromBody] T value)
        {
            try
            {
                return Ok(await _genericService.Update(id, value));
            }
            catch (Exception)
            {
                return BadRequest(_genericService.Error);
            }
        }

        // DELETE api/<GenericController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> Delete(int id)
        {
            try
            {
                return Ok(await _genericService.Delete(id));
            }
            catch (Exception)
            {
                return BadRequest(_genericService.Error);
            }
        }
    }
}
