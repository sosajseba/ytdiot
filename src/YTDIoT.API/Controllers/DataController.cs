using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using YTDIoT.API.Extensions;
using YTDIoT.API.Helpers;
using YTDIoT.API.Response;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Filters;
using YTDIoT.COMMON.Interfaces;
using YTDIoT.COMMON.Requests;

namespace YTDIoT.API.Controllers
{
    [Authorize(Roles = "Admin,User")]
    [Route("api/[controller]")]
    [ApiController]
    public class DataController :ControllerBase
    {
        private readonly IDataService _dataService;
        private readonly IMapper _mapper;
        private readonly IUriService _uriService;
        public DataController(IDataService dataService, IMapper mapper, IUriService uriService)
        {
            _dataService = dataService;
            _uriService = uriService;
            _mapper = mapper;
        }

        // GET api/device
        [HttpGet]
        public async Task<ActionResult<Data>> Get([FromQuery] DataQuery getQuery, [FromQuery] PaginationQuery paginationQuery)
        {
            try
            {
                int loggedUserId = HttpContext.GetUserId();
                bool channelExists = await _dataService.ChannelExists(getQuery.IdChannel);
                bool userOwnsChannel = await _dataService.UserOwnsChannel(loggedUserId, getQuery.IdChannel);
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var filter = _mapper.Map<DataFilter>(getQuery);

                if (channelExists)
                {
                    if (userOwnsChannel)
                    {
                        var data = await _dataService.GetAll(filter, pagination);
                        if (pagination == null || pagination.PageNumber < 1 || pagination.PageSize < 1)
                        {
                            return Ok(new PagedResponse<Data>(data));
                        }
                        var paginationResponse = PaginationHelper.CreatePaginationResponse<Data>(_uriService, pagination, data);
                        return Ok(paginationResponse);
                    }
                    return Forbid();
                }
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest(_dataService.Error);
            }
        }
        
        // GET api/<GenericController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Data>> Get(int id)
        {
            try
            {
                var data = await _dataService.Get(id);
                int loggedUserId = HttpContext.GetUserId();
                if (data != null)
                {
                    bool userOwnsChannel = await _dataService.UserOwnsChannel(loggedUserId, data.IdChannel);
                    if (userOwnsChannel)
                    {
                        return Ok(data);
                    }
                    return Forbid();
                }
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest(_dataService.Error);
            }
        }

        // POST api/<GenericController>
        [HttpPost]
        public async Task<ActionResult<Data>> Post([FromBody] Data value)
        {
            try
            {
                int loggedUserId = HttpContext.GetUserId();
                bool userOwnsChannel = await _dataService.UserOwnsChannel(loggedUserId, value.IdChannel);
                if (userOwnsChannel)
                {
                    return Ok(await _dataService.Create(value));
                }
                return Forbid();
            }
            catch (Exception)
            {
                return BadRequest(_dataService.Error);
            }
        }

        // PUT api/<GenericController>/5
        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<Data>> Put(int id, [FromBody] Data value)
        {
            try
            {
                return Ok(await _dataService.Update(id, value));
            }
            catch (Exception)
            {
                return BadRequest(_dataService.Error);
            }
        }

        // DELETE api/<GenericController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> Delete(int id)
        {
            try
            {
                var data = await _dataService.Get(id);
                int loggedUserId = HttpContext.GetUserId();
                if (data != null)
                {
                    bool userOwnsChannel = await _dataService.UserOwnsChannel(loggedUserId, data.IdChannel);
                    if (userOwnsChannel)
                    {
                        return Ok(await _dataService.Delete(id));
                    }
                    return Forbid();
                }
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest(_dataService.Error);
            }
        }
    }
}
