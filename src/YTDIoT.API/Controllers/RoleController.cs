﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Interfaces;

namespace YTDIoT.API.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService _roleService;
        public RoleController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        // GET api/user
        [HttpGet]
        public async Task<ActionResult<Role>> Get()
        {
            try
            {
                return Ok(await _roleService.GetAll());
            }
            catch (Exception)
            {
                return BadRequest(_roleService.Error);
            }
        }

        // GET api/<GenericController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Role>> Get(int id)
        {
            try
            {
                return Ok(await _roleService.Get(id));
            }
            catch (Exception)
            {
                return BadRequest(_roleService.Error);
            }
        }

        // POST api/<GenericController>
        [HttpPost]
        public async Task<ActionResult<Role>> Post([FromBody] Role value)
        {
            try
            {
                return Ok(await _roleService.Create(value));
            }
            catch (Exception)
            {
                return BadRequest(_roleService.Error);
            }
        }

        // PUT api/<GenericController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult<Role>> Put(int id, [FromBody] Role value)
        {
            try
            {
                return Ok(await _roleService.Update(id, value));
            }
            catch (Exception)
            {
                return BadRequest(_roleService.Error);
            }
        }

        // DELETE api/<GenericController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> Delete(int id)
        {
            try
            {
                return Ok(await _roleService.Delete(id));
            }
            catch (Exception)
            {
                return BadRequest(_roleService.Error);
            }
        }
    }
}
