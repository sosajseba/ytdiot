using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using YTDIoT.API.Extensions;
using YTDIoT.API.Helpers;
using YTDIoT.API.Response;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Filters;
using YTDIoT.COMMON.Interfaces;
using YTDIoT.COMMON.Requests;

namespace YTDIoT.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeviceController : ControllerBase
    {
        private readonly IDeviceService _deviceService;
        private readonly IMapper _mapper;
        private readonly IUriService _uriService;
        public DeviceController(IDeviceService deviceService, IMapper mapper, IUriService uriService)
        {
            _deviceService = deviceService;
            _uriService = uriService;
            _mapper = mapper;
        }

        // GET api/device
        [HttpGet]
        [Authorize(Roles = "Admin,User")]
        public async Task<ActionResult<Device>> Get([FromQuery] DeviceQuery getQuery, [FromQuery] PaginationQuery paginationQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var filter = _mapper.Map<DeviceFilter>(getQuery);
                var devices = await _deviceService.GetAll(filter, pagination);
                if (pagination == null || pagination.PageNumber < 1 || pagination.PageSize < 1)
                {
                    return Ok(new PagedResponse<Device>(devices));
                }
                var paginationResponse = PaginationHelper.CreatePaginationResponse<Device>(_uriService, pagination, devices);
                return Ok(paginationResponse);
            }
            catch (Exception)
            {
                return BadRequest(_deviceService.Error);
            }
        }

        // GET api/<GenericController>/5
        [HttpGet("{id}")]
        [Authorize(Roles = "Admin,User")]
        public async Task<ActionResult<Device>> Get(int id)
        {
            try
            {
                var device = await _deviceService.Get(id);
                int loggedUserId = HttpContext.GetUserId();
                if (device != null)
                {
                    bool userOwnsDevice = await _deviceService.UserOwnsDevice(loggedUserId, id);
                    if (userOwnsDevice)
                    {
                        return Ok(device);
                    }
                    return Forbid();
                }
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest(_deviceService.Error);
            }
        }

        // POST api/<GenericController>
        [HttpPost]
        [Authorize(Roles = "Admin,User")]
        public async Task<ActionResult<Device>> Post([FromBody] Device value)
        {
            try
            {
                value.IdUser = HttpContext.GetUserId();
                return Ok(await _deviceService.Create(value));
            }
            catch (Exception)
            {
                return BadRequest(_deviceService.Error);
            }
        }

        // PUT api/<GenericController>/5
        [HttpPut("{id}")]
        [Authorize(Roles = "Admin,User")]
        public async Task<ActionResult<Device>> Put(int id, [FromBody] Device value)
        {
            try
            {
                var device = await _deviceService.Get(id);
                int loggedUserId = HttpContext.GetUserId();
                if (device != null)
                {
                    bool userOwnsDevice = await _deviceService.UserOwnsDevice(loggedUserId, id);
                    if (userOwnsDevice)
                    {
                        return Ok(await _deviceService.Update(id, value));
                    }
                    return Forbid();
                }
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest(_deviceService.Error);
            }
        }

        // DELETE api/<GenericController>/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin,User")]
        public async Task<ActionResult<bool>> Delete(int id)
        {
            try
            {
                var device = await _deviceService.Get(id);
                int loggedUserId = HttpContext.GetUserId();
                if (device != null)
                {
                    bool userOwnsDevice = await _deviceService.UserOwnsDevice(loggedUserId, id);
                    if (userOwnsDevice)
                    {
                        return Ok(await _deviceService.Delete(id));
                    }
                    return Forbid();
                }
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest(_deviceService.Error);
            }
        }
    }
}
