﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using YTDIoT.API.Extensions;
using YTDIoT.API.Helpers;
using YTDIoT.API.Response;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Filters;
using YTDIoT.COMMON.Interfaces;
using YTDIoT.COMMON.Requests;

namespace YTDIoT.API.Controllers
{
    [Authorize(Roles = "Admin,User")]
    [Route("api/[controller]")]
    [ApiController]
    public class ChannelController : ControllerBase
    {
        private readonly IChannelService _channelService;
        private readonly IMapper _mapper;
        private readonly IUriService _uriService;
        public ChannelController(IChannelService channelService, IMapper mapper, IUriService uriService)
        {
            _channelService = channelService;
            _uriService = uriService;
            _mapper = mapper;
        }

        // GET api/user
        [HttpGet]
        public async Task<ActionResult<Channel>> Get([FromQuery] ChannelQuery getQuery, [FromQuery] PaginationQuery paginationQuery)
        {
            try
            {
                getQuery.IdUser = HttpContext.GetUserId();
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var filter = _mapper.Map<ChannelFilter>(getQuery);
                var channels = await _channelService.GetAll(filter, pagination);
                if (pagination == null || pagination.PageNumber < 1 || pagination.PageSize < 1)
                {
                    return Ok(new PagedResponse<Channel>(channels));
                }
                var paginationResponse = PaginationHelper.CreatePaginationResponse<Channel>(_uriService, pagination, channels);
                return Ok(paginationResponse);
            }
            catch (Exception)
            {
                return BadRequest(_channelService.Error);
            }
        }

        // GET api/role/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Channel>> Get(int id)
        {
            try
            {
                var channel = await _channelService.Get(id);
                int loggedUserId = HttpContext.GetUserId();
                if (channel != null)
                {
                    bool userOwnsChannel = await _channelService.UserOwnsChannel(loggedUserId, id);
                    if (userOwnsChannel)
                    {
                        return Ok(channel);
                    }
                    return Forbid();
                }
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest(_channelService.Error);
            }
        }

        // POST api/role
        [HttpPost]
        public async Task<ActionResult<Channel>> Post([FromBody] Channel value)
        {
            try
            {
                int loggedUserId = HttpContext.GetUserId();
                bool userOwnsDevice = await _channelService.UserOwnsDevice(loggedUserId, value.IdDevice);
                if (userOwnsDevice)
                {
                    return Ok(await _channelService.Create(value));
                }
                return Forbid();
            }
            catch (Exception)
            {
                return BadRequest(_channelService.Error);
            }
        }

        // PUT api/role/5
        [HttpPut("{id}")]
        public async Task<ActionResult<Channel>> Put(int id, [FromBody] Channel value)
        {
            try
            {
                var channel = await _channelService.Get(id);
                int loggedUserId = HttpContext.GetUserId();
                if (channel != null)
                {
                    bool userOwnsChannel = await _channelService.UserOwnsChannel(loggedUserId, id);
                    if (userOwnsChannel)
                    {
                        value.IdDevice = channel.IdDevice;
                        return Ok(await _channelService.Update(id, value));
                    }
                    return Forbid();
                }
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest(_channelService.Error);
            }
        }

        // DELETE api/role/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> Delete(int id)
        {
            try
            {
                var channel = await _channelService.Get(id);
                int loggedUserId = HttpContext.GetUserId();
                if (channel != null)
                {
                    bool userOwnsChannel = await _channelService.UserOwnsChannel(loggedUserId, id);
                    if (userOwnsChannel)
                    {
                        return Ok(await _channelService.Delete(id));
                    }
                    return Forbid();
                }
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest(_channelService.Error);
            }
        }
    }
}
