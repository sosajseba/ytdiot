using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using YTDIoT.API.Extensions;
using YTDIoT.API.Helpers;
using YTDIoT.API.Response;
using YTDIoT.COMMON;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Filters;
using YTDIoT.COMMON.Interfaces;
using YTDIoT.COMMON.Requests;

namespace YTDIoT.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly IUriService _uriService;
        public UserController(IUserService userService, IMapper mapper, IUriService uriService)
        {
            _uriService = uriService;
            _userService = userService;
            _mapper = mapper;
        }

        // GET api/user
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<User>> Get([FromQuery] UserQuery getQuery, [FromQuery] PaginationQuery paginationQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var filter = _mapper.Map<UserFilter>(getQuery);
                var users = await _userService.GetAll(filter, pagination);

                if (pagination == null || pagination.PageNumber < 1 || pagination.PageSize < 1)
                {
                    return Ok(new PagedResponse<User>(users));
                }
                var paginationResponse = PaginationHelper.CreatePaginationResponse<User>(_uriService, pagination, users);
                
                return Ok(paginationResponse);
            }
            catch (Exception)
            {
                return BadRequest(_userService.Error);
            }
        }

        // GET api/user/5
        [HttpGet("{id}")]
        [Authorize(Roles = "Admin,User")]
        public async Task<ActionResult<UserDto>> Get(int id)
        {
            try
            {
                int loggedUserId = HttpContext.GetUserId();
                string loggedUserRole = HttpContext.GetUserRole();
                if (loggedUserRole.Equals("Admin"))
                {
                    var user = _mapper.Map<UserDto>(await _userService.Get(id));
                    return Ok(user);
                }
                else
                {
                    if (loggedUserId == id)
                    {
                        var user = _mapper.Map<UserDto>(await _userService.Get(id));
                        return Ok(user);
                    }
                }
                return Unauthorized();
            }
            catch (Exception)
            {
                return BadRequest(_userService.Error);
            }
        }

        // POST api/user
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<User>> Post([FromBody] User value)
        {
            try
            {
                return Ok(await _userService.Create(value));
            }
            catch (Exception)
            {
                return BadRequest(_userService.Error);
            }
        }

        // PUT api/user/5
        [HttpPut("{id}")]
        [Authorize(Roles = "Admin,User")]
        public async Task<ActionResult<User>> Put(int id, [FromBody] User value)
        {
            try
            {
                int loggedUserId = HttpContext.GetUserId();
                if (loggedUserId == id)
                {
                    return Ok(await _userService.Create(value));
                }
                return Unauthorized();
            }
            catch (Exception)
            {
                return BadRequest(_userService.Error);
            }
        }

        // DELETE api/user/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin,User")]
        public async Task<ActionResult<bool>> Delete(int id)
        {
            try
            {
                int loggedUserId = HttpContext.GetUserId();
                if (loggedUserId == id)
                {
                    return Ok(await _userService.Delete(id));
                }
                return Unauthorized();
            }
            catch (Exception)
            {
                return BadRequest(_userService.Error);
            }
        }
    }
}