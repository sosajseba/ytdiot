﻿using AutoMapper;
using YTDIoT.COMMON;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Filters;
using YTDIoT.COMMON.Requests;

namespace YTDIoT.API.Mapping
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<PaginationFilter, PaginationQuery>().ReverseMap();
            CreateMap<UserFilter, UserQuery>().ReverseMap();
            CreateMap<DeviceFilter, DeviceQuery>().ReverseMap();
            CreateMap<ChannelFilter, ChannelQuery>().ReverseMap();
            CreateMap<DataFilter, DataQuery>().ReverseMap();
            CreateMap<User, UserDto>().ForMember(x => x.Password, opt => opt.Ignore());
            CreateMap<UserDto, User>();
        }
    }
}
