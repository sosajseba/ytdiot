﻿using System.Collections.Generic;
using System.Linq;
using YTDIoT.API.Response;
using YTDIoT.COMMON.Filters;
using YTDIoT.COMMON.Interfaces;
using YTDIoT.COMMON.Requests;

namespace YTDIoT.API.Helpers
{
    public class PaginationHelper
    {
        public static PagedResponse<T> CreatePaginationResponse<T>(IUriService uriService, PaginationFilter paginationFilter, IEnumerable<T> data)
        {
            var route = "api/" + typeof(T).Name + "/";
            var nextPage = paginationFilter.PageNumber >= 1
                    ? uriService.GetAllUserUri(route, new PaginationQuery(paginationFilter.PageNumber + 1, paginationFilter.PageSize)).ToString()
                    : null;
            var previousPage = paginationFilter.PageNumber - 1 >= 1
                    ? uriService.GetAllUserUri(route, new PaginationQuery(paginationFilter.PageNumber - 1, paginationFilter.PageSize)).ToString()
                    : null;
            return new PagedResponse<T>
            {
                Data = data,
                PageNumber = paginationFilter.PageNumber >= 1 ? paginationFilter.PageNumber : (int?)null,
                PageSize = paginationFilter.PageSize >= 1 ? paginationFilter.PageSize : (int?)null,
                NextPage = data.Any() ? nextPage : null,
                PreviousPage = previousPage
            };
        }
    }
}
