﻿using Microsoft.AspNetCore.Http;
using System;
using System.Linq;

namespace YTDIoT.API.Extensions
{
    public static class GenericExtension
    {
        public static int GetUserId(this HttpContext httpContext)
        {
            if (httpContext.User == null)
            {
                return 0;
            }
            var id = httpContext.User.Claims.Single(x => x.Type == "id").Value;

            return Int32.Parse(id);
        }
        public static string GetUserRole(this HttpContext httpContext)
        {
            if (httpContext.User == null)
            {
                return string.Empty;
            }
            var role = httpContext.User.Claims.Single(x => x.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role").Value;

            return role;
        }
    }
}
