﻿using System;

namespace YTDIoT.COMMON.Filters
{
    public class DataFilter
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int IdChannel { get; set; }
    }
}
