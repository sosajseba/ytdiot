﻿namespace YTDIoT.COMMON.Filters
{
    public class DeviceFilter
    {
        public string Name { get; set; }
        public string Location { get; set; }
    }
}
