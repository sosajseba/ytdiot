﻿namespace YTDIoT.COMMON.Filters
{
    public class UserFilter
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
    }
}
