namespace YTDIoT.COMMON.Entities
{
    public class Data : BaseEntity
    {
        public int IdChannel { get; set; }
        public bool State { get; set; }
    }
}