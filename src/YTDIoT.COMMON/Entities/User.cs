namespace YTDIoT.COMMON.Entities
{
    public class User : BaseEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Ip { get; set; }
        public string Password { get; set; }
        public int IdRole { get; set; }
    }
}