using System;

namespace YTDIoT.COMMON.Entities
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
    }
}
