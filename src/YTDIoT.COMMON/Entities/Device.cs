namespace YTDIoT.COMMON.Entities
{
    public class Device : BaseEntity
    {
        public int IdUser { get; set; }
        public string Name { get; set; }
        public string SerialNum { get; set; }
        public string Location { get; set; }
    }
}