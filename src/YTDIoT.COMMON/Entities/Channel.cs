﻿namespace YTDIoT.COMMON.Entities
{
    public class Channel : BaseEntity
    {
        public string Name { get; set; }
        public bool State { get; set; }
        public int IdDevice { get; set; }
    }
}
