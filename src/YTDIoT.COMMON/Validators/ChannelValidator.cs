﻿using FluentValidation;
using YTDIoT.COMMON.Entities;

namespace YTDIoT.COMMON.Validators
{
    public class ChannelValidator : GenericValidator<Channel>
    {
        public ChannelValidator()
        {
            RuleFor(a => a.Date).NotEmpty().NotNull();
            RuleFor(a => a.IdDevice).NotEmpty().NotNull();
            RuleFor(a => a.State).NotNull();
            RuleFor(a => a.Name).NotEmpty().NotNull().Length(1,50);
        }
    }
}
