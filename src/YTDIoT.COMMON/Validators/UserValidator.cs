using FluentValidation;
using YTDIoT.COMMON.Entities;

namespace YTDIoT.COMMON.Validators
{
    public class UserValidator : GenericValidator<User>
    {
        public UserValidator()
        {
            RuleFor(a => a.Email).NotEmpty().NotNull().EmailAddress();
            RuleFor(a => a.Name).NotEmpty().NotNull().Length(1, 50);
            RuleFor(a => a.Password).NotEmpty().NotNull().Length(5, 50);
            RuleFor(a => a.Ip).NotEmpty().NotNull();
            RuleFor(a => a.IdRole).NotEmpty().NotNull();
            RuleFor(a => a.Date).NotEmpty().NotNull();

        }
    }
}