using FluentValidation;
using YTDIoT.COMMON.Entities;

namespace YTDIoT.COMMON.Validators
{
    public class DeviceValidator : GenericValidator<Device>
    {
        public DeviceValidator()
        {
            RuleFor(a => a.IdUser).NotEmpty().NotNull();
            RuleFor(a => a.Name).NotEmpty().NotNull().Length(3,50);
            RuleFor(a => a.SerialNum).NotEmpty().NotNull().Length(3, 50);
            RuleFor(a => a.Location).NotEmpty().NotNull().Length(3, 50);
            RuleFor(a => a.Date).NotEmpty().NotNull();
        }
    }
}