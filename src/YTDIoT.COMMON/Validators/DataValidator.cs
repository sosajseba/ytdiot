using FluentValidation;
using YTDIoT.COMMON.Entities;

namespace YTDIoT.COMMON.Validators
{
    public class DataValidator : GenericValidator<Data>
    {
        public DataValidator()
        {
            RuleFor(a => a.Date).NotEmpty().NotNull();
            RuleFor(a => a.IdChannel).NotEmpty().NotNull();
            RuleFor(a => a.State).NotEmpty().NotNull();
        }
    }
}