﻿using FluentValidation;
using YTDIoT.COMMON.Entities;

namespace YTDIoT.COMMON.Validators
{
    public class RoleValidator : GenericValidator<Role>
    {
        public RoleValidator()
        {
            RuleFor(a => a.Name).NotEmpty().NotNull().Length(1, 50);
            RuleFor(a => a.Date).NotEmpty().NotNull();

        }
    }
}
