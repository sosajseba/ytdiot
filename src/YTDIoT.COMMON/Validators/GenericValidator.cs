using FluentValidation;
using YTDIoT.COMMON.Entities;

namespace YTDIoT.COMMON.Validators
{
    public class GenericValidator<T> : AbstractValidator<T> where T : BaseEntity
    {
        public GenericValidator()
        {
            RuleFor(e => e.Id).NotNull();
            RuleFor(e => e.Date).NotEmpty().NotNull();
        }
    }
}