﻿using System;

namespace YTDIoT.COMMON
{
    public class UserDto
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Ip { get; set; }
        public string Password { get; set; }
        public int IdRole { get; set; }
    }
}
