﻿namespace YTDIoT.COMMON.Requests
{
    public class UserQuery
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
    }
}
