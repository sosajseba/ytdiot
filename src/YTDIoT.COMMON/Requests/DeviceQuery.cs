﻿namespace YTDIoT.COMMON.Requests
{
    public class DeviceQuery
    {
        public string Name { get; set; }
        public string Location { get; set; }
    }
}
