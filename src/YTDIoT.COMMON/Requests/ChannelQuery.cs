﻿namespace YTDIoT.COMMON.Requests
{
    public class ChannelQuery
    {
        public string Name { get; set; }
        public bool State { get; set; }
        public int IdDevice { get; set; }
        public int IdUser { get; set; }
    }
}
