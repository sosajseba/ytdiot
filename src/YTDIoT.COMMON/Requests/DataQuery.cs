﻿using System;

namespace YTDIoT.COMMON.Requests
{
    public class DataQuery
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int IdChannel { get; set; }
    }
}
