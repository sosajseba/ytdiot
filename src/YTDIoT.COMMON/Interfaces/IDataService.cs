using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Filters;

namespace YTDIoT.COMMON.Interfaces
{
    public interface IDataService
    {
        string Error { get; }
        Task<IEnumerable<Data>> GetAll(DataFilter filter, PaginationFilter pagination);
        Task<Data> Get(int id);
        Task<int> Create(Data entity);
        Task<int> Update(int id, Data entity);
        Task<int> Delete(int id);
        Task<IEnumerable<Data>> Query(string sql);
        Task<bool> UserOwnsChannel(int loggedUserId, int idChannel);
        Task<bool> ChannelExists(int idChannel);
    }
}