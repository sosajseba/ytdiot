using System.Collections.Generic;
using System.Threading.Tasks;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Filters;

namespace YTDIoT.COMMON.Interfaces
{
    public interface IDeviceService
    {
        string Error { get; }
        Task<IEnumerable<Device>> GetAll(DeviceFilter deviceFilter, PaginationFilter paginationFilter);
        Task<Device> Get(int id);
        Task<int> Create(Device entity);
        Task<int> Update(int id, Device entity);
        Task<int> Delete(int id);
        Task<IEnumerable<Device>> Query(string sql);
        Task<bool> UserOwnsDevice(int loggedUserId, int id);
    }
}