﻿using YTDIoT.COMMON.Entities;

namespace YTDIoT.COMMON.Interfaces
{
    public interface IChannelRepository : IGenericRepository<Channel>
    {
    }
}
