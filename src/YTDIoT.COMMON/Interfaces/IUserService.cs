using System.Collections.Generic;
using System.Threading.Tasks;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Filters;

namespace YTDIoT.COMMON.Interfaces
{
    public interface IUserService
    {
        string Error { get; }
        Task<IEnumerable<User>> GetAll(UserFilter userFilter, PaginationFilter paginationFilter);
        Task<User> Get(int id);
        Task<int> Create(User entity);
        Task<int> Update(int id, User entity);
        Task<int> Delete(int id);
        Task<IEnumerable<User>> Query(string sql);
        Task<int> ValidateUser(string email, string password);
        Task<Role> GetRole(int roleId);
    }
}
