﻿using YTDIoT.COMMON.Entities;

namespace YTDIoT.COMMON.Interfaces
{
    public interface IDeviceRepository : IGenericRepository<Device>
    {
    }
}
