﻿namespace YTDIoT.COMMON.Interfaces
{
    public interface IUnitOfWork
    {
        IUserRepository UserRepository { get; }
        IRoleRepository RoleRepository { get; }
        IDataRepository DataRepository { get; }
        IDeviceRepository DeviceRepository { get; }
        IChannelRepository ChannelRepository { get; }
    }
}
