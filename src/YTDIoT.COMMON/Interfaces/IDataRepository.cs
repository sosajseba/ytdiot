﻿using YTDIoT.COMMON.Entities;

namespace YTDIoT.COMMON.Interfaces
{
    public interface IDataRepository : IGenericRepository<Data>
    {
    }
}
