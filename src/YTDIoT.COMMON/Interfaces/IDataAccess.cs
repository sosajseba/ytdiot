using System.Collections.Generic;
using System.Threading.Tasks;

namespace YTDIoT.COMMON.Interfaces
{
    public interface IDataAccess
    {
        Task<List<T>> LoadData<T, U>(string sql, U parameters, string connectionString);
        Task<int> SaveData<T>(string sql, T parameters, string connectionString);
    }
}
