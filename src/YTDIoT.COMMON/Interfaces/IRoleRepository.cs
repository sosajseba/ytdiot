﻿using YTDIoT.COMMON.Entities;

namespace YTDIoT.COMMON.Interfaces
{
    public interface IRoleRepository : IGenericRepository<Role>
    {
    }
}
