using System.Collections.Generic;
using System.Threading.Tasks;
using YTDIoT.COMMON.Entities;

namespace YTDIoT.COMMON.Interfaces
{
    public interface IGenericRepository<T> where T : BaseEntity
    {
        string Error { get; }
        Task<T> Get(int id);
        Task<IEnumerable<T>> GetAll();
        Task<int> Create(T entity);
        Task<int> Delete(int id);
        Task<int> Update(int id, T entity);
        Task<IEnumerable<T>> Query(string sql);
    }
}