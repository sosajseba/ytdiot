﻿using System.Collections.Generic;
using System.Threading.Tasks;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Filters;

namespace YTDIoT.COMMON.Interfaces
{
    public interface IChannelService
    {
        string Error { get; }
        Task<IEnumerable<Channel>> GetAll(ChannelFilter deviceFilter, PaginationFilter paginationFilter);
        Task<Channel> Get(int id);
        Task<int> Create(Channel entity);
        Task<int> Update(int id, Channel entity);
        Task<int> Delete(int id);
        Task<IEnumerable<Channel>> Query(string sql);
        Task<bool> UserOwnsChannel(int loggedUserId, int id);
        Task<bool> UserOwnsDevice(int loggedUserId, int id);
    }
}
