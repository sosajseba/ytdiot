﻿using YTDIoT.COMMON.Entities;

namespace YTDIoT.COMMON.Interfaces
{
    public interface IUserRepository : IGenericRepository<User>
    {
    }
}
