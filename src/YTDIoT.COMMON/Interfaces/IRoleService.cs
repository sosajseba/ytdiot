﻿using System.Collections.Generic;
using System.Threading.Tasks;
using YTDIoT.COMMON.Entities;

namespace YTDIoT.COMMON.Interfaces
{
    public interface IRoleService
    {
        string Error { get; }
        Task<IEnumerable<Role>> GetAll();
        Task<Role> Get(int id);
        Task<int> Create(Role entity);
        Task<int> Update(int id, Role entity);
        Task<int> Delete(int id);
        Task<IEnumerable<Role>> Query(string sql);
    }
}
