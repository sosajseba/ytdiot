﻿using System;
using YTDIoT.COMMON.Requests;

namespace YTDIoT.COMMON.Interfaces  
{
    public interface IUriService
    {
        Uri GetUserUri(int userId);
        Uri GetAllUserUri(string route, PaginationQuery pagination = null);
    }
}
