﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Filters;
using YTDIoT.COMMON.Interfaces;

namespace YTDIoT.BIZ.Services
{
    public class ChannelService : IChannelService
    {
        private readonly IUnitOfWork _unitOfWork;
        public string Error => _unitOfWork.ChannelRepository.Error;
        public ChannelService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Channel>> GetAll(ChannelFilter filter, PaginationFilter paginationFilter)
        {
            try
            {
                int offset = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

                string pagination = " LIMIT " + paginationFilter.PageSize + " OFFSET " + offset;

                string sql = "SELECT Channel.Id,Channel.Name,Channel.State,Channel.Date,Channel.IdDevice " +
                    "FROM Channel INNER JOIN Device ON Device.Id = Channel.IdDevice " +
                    "INNER JOIN User ON User.Id = Device.IdUser where User.Id = " + filter.IdUser;

                if (!string.IsNullOrEmpty(filter.Name))
                {

                    sql += " and Channel.Name like '%" + filter.Name + "%' ";

                }

                if (filter.IdDevice != 0)
                {
                    sql += " and Channel.IdDevice =" + filter.IdDevice;

                }

                if (filter.State)
                {

                    sql += " and Channel.State = 1";

                }
                if (!filter.State)
                {
                    sql += " and Channel.State = 0";

                }

                sql += pagination;

                return await _unitOfWork.ChannelRepository.Query(sql);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Task<Channel> Get(int id)
        {
            try
            {
                return _unitOfWork.ChannelRepository.Get(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<int> Create(Channel entity)
        {
            try
            {
                var device = await _unitOfWork.DeviceRepository.Get(entity.IdDevice);

                if (device != null)
                {
                    return await _unitOfWork.ChannelRepository.Create(entity);
                }

                return 0;
                
            }
            catch (Exception)
            {
                throw;
            }

        }
        public Task<int> Update(int id, Channel entity)
        {
            try
            {
                return _unitOfWork.ChannelRepository.Update(id, entity);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public Task<int> Delete(int id)
        {
            try
            {
                return _unitOfWork.ChannelRepository.Delete(id);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public Task<IEnumerable<Channel>> Query(string sql)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> UserOwnsChannel(int loggedUserId, int channelId)
        {
            string sql = "SELECT Channel.Id FROM Channel " +
                "INNER JOIN Device ON Device.Id = Channel.IdDevice " +
                "INNER JOIN User ON User.Id = Device.IdUser where User.Id = " + loggedUserId +
                " and Channel.Id = " + channelId;
            var row = await _unitOfWork.ChannelRepository.Query(sql);
            if (row.Any())
            {
                return true;
            }
            return false;
        }

        public async Task<bool> UserOwnsDevice(int loggedUserId, int deviceId)
        {
            string sql = "SELECT Device.Id FROM Device " +
                "INNER JOIN User ON User.Id = Device.IdUser where User.Id = " + loggedUserId + " and Device.Id = " + deviceId;
            var row = await _unitOfWork.DeviceRepository.Query(sql);
            if (row.Any())
            {
                return true;
            }
            return false;
        }
    }
}
