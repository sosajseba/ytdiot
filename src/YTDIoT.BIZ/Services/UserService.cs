using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Filters;
using YTDIoT.COMMON.Interfaces;

namespace YTDIoT.BIZ.Services
{

    public class UserService : IUserService
    {
        
        private readonly IUnitOfWork _unitOfWork;
        public string Error => _unitOfWork.UserRepository.Error;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<User>> GetAll(UserFilter userFilter, PaginationFilter paginationFilter)
        {
            try
            {
                int offset = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;
                bool isWhere = false;

                string pagination = " LIMIT " + paginationFilter.PageSize + " OFFSET " + offset;

                string sql = "select * from User";

                if (!string.IsNullOrEmpty(userFilter.Name))
                {
                    if (!isWhere)
                    {
                        sql += " where Name like '%" + userFilter.Name + "%' ";
                        isWhere = true;
                    }
                    else
                    {
                        sql += " and Name like '%" + userFilter.Name + "%' ";
                    }
                }

                if (!string.IsNullOrEmpty(userFilter.Email))
                {
                    if (!isWhere)
                    {
                        sql += " where Email like '%" + userFilter.Email + "%' ";
                        isWhere = true;
                    }
                    else
                    {
                        sql += " and Email like '%" + userFilter.Email + "%' ";
                    }
                }

                if (!string.IsNullOrEmpty(userFilter.Role))
                {
                    string roleQuery = "select id from Role where name like '%" + userFilter.Role + "%'";
                    var role = await _unitOfWork.RoleRepository.Query(roleQuery);
                    if (!isWhere)
                    {
                        sql += " where IdRole = "+ role.FirstOrDefault().Id;
                        isWhere = true;
                    }
                    else
                    {
                        sql += " and IdRole = " + role.FirstOrDefault().Id;
                    }
                }

                sql += pagination;

                return await _unitOfWork.UserRepository.Query(sql);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Task<User> Get(int id)
        {
            try
            {
                return _unitOfWork.UserRepository.Get(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Task<int> Create(User entity)
        {
            try
            {
                return _unitOfWork.UserRepository.Create(entity);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public Task<int> Update(int id, User entity)
        {
            try
            {
                return _unitOfWork.UserRepository.Update(id, entity);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public Task<int> Delete(int id)
        {
            try
            {
                return _unitOfWork.UserRepository.Delete(id);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public Task<IEnumerable<User>> Query(string sql)
        {
            throw new NotImplementedException();
        }

        public async Task<int> ValidateUser(string email, string password)
        {
            string sql = "select User.Id from User where User.Email = '" + email + 
                "' and User.Password = '" + password + "'";  
            var rows = await _unitOfWork.UserRepository.Query(sql);
            
            if (rows.Any())
            {
                return rows.FirstOrDefault().Id;
            }

            return 0;
        }

        public async Task<Role> GetRole(int roleId)
        {
            var role = await _unitOfWork.RoleRepository.Get(roleId);

            return role;
        }
    }
}