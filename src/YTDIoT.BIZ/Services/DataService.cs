using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Filters;
using YTDIoT.COMMON.Interfaces;

namespace YTDIoT.BIZ.Services
{
    public class DataService : IDataService
    {
        private readonly IUnitOfWork _unitOfWork;
        public string Error => _unitOfWork.DataRepository.Error;
        public DataService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Data>> GetAll(DataFilter dataFilter, PaginationFilter paginationFilter)
        {
            try
            {
                int offset = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

                string pagination = " LIMIT " + paginationFilter.PageSize + " OFFSET " + offset;

                string sql = "SELECT Data.Id,Data.Date,Data.State,Data.IdChannel FROM Data " +
                    "INNER JOIN Channel ON Channel.Id = Data.IdChannel " +
                    "INNER JOIN Device ON Device.Id = Channel.IdDevice " +
                    "INNER JOIN User ON User.Id = Device.IdUser where Data.IdChannel = " + dataFilter.IdChannel;

                if (dataFilter.FromDate.HasValue)
                {
                    sql += " AND Data.Date > '" + dataFilter.FromDate.Value.ToString("yyyy-MM-dd H:mm:ss") + "'";
                }

                if (dataFilter.ToDate.HasValue)
                {
                    sql += " AND Data.Date < '" + dataFilter.ToDate.Value.ToString("yyyy-MM-dd H:mm:ss") + "'";
                }

                sql += pagination;

                return await _unitOfWork.DataRepository.Query(sql);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Task<Data> Get(int id)
        {
            try
            {
                return _unitOfWork.DataRepository.Get(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Task<int> Create(Data entity)
        {
            try
            {
                return _unitOfWork.DataRepository.Create(entity);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public Task<int> Update(int id, Data entity)
        {
            try
            {
                return _unitOfWork.DataRepository.Update(id, entity);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public Task<int> Delete(int id)
        {
            try
            {
                return _unitOfWork.DataRepository.Delete(id);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public Task<IEnumerable<Data>> Query(string sql)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> UserOwnsChannel(int loggedUserId, int idChannel)
        {
            string sql = "SELECT Channel.Id FROM Channel " +
                "INNER JOIN Device ON Device.Id = Channel.IdDevice " +
                "INNER JOIN User ON User.Id = Device.IdUser where User.Id = " + loggedUserId +
                " and Channel.Id = " + idChannel;
            var row = await _unitOfWork.ChannelRepository.Query(sql);
            if (row.Any())
            {
                return true;
            }
            return false;
        }

        public async Task<bool> ChannelExists(int idChannel)
        {
            var channel = await _unitOfWork.ChannelRepository.Get(idChannel);
            if (channel != null)
            {
                return true;
            }
            return false;
        }
    }
}