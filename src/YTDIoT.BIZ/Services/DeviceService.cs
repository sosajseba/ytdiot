using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Filters;
using YTDIoT.COMMON.Interfaces;

namespace YTDIoT.BIZ.Services
{
    public class DeviceService : IDeviceService
    {
        private readonly IUnitOfWork _unitOfWork;
        public string Error => _unitOfWork.DeviceRepository.Error;
        public DeviceService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Device>> GetAll(DeviceFilter deviceFilter, PaginationFilter paginationFilter)
        {
            try
            {
                int offset = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;
                bool isWhere = false;

                string pagination = " LIMIT " + paginationFilter.PageSize + " OFFSET " + offset;

                string sql = "select * from Device";

                if (!string.IsNullOrEmpty(deviceFilter.Name))
                {
                    if (!isWhere)
                    {
                        sql += " where Name like '%" + deviceFilter.Name + "%' ";
                        isWhere = true;
                    }
                    else
                    {
                        sql += " and Name like '%" + deviceFilter.Name + "%' ";
                    }
                }

                if (!string.IsNullOrEmpty(deviceFilter.Location))
                {
                    if (!isWhere)
                    {
                        sql += " where Location like '%" + deviceFilter.Location + "%' ";
                        isWhere = true;
                    }
                    else
                    {
                        sql += " and Location like '%" + deviceFilter.Location + "%' ";
                    }
                }

                sql += pagination;

                return await _unitOfWork.DeviceRepository.Query(sql);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Task<Device> Get(int id)
        {
            try
            {
                return _unitOfWork.DeviceRepository.Get(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Task<int> Create(Device entity)
        {
            try
            {
                return _unitOfWork.DeviceRepository.Create(entity);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public Task<int> Update(int id, Device entity)
        {
            try
            {
                return _unitOfWork.DeviceRepository.Update(id, entity);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public Task<int> Delete(int id)
        {
            try
            {
                return _unitOfWork.DeviceRepository.Delete(id);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public Task<IEnumerable<Device>> Query(string sql)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> UserOwnsDevice(int loggedUserId, int deviceId)
        {
            string sql = "SELECT Device.Id FROM Device " +
                "INNER JOIN User ON User.Id = Device.IdUser where User.Id = " + loggedUserId + " and Device.Id = " + deviceId;
            var row = await _unitOfWork.DeviceRepository.Query(sql);
            if (row.Any())
            {
                return true;
            }
            return false;
        }
    }
}