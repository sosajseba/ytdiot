﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Interfaces;

namespace YTDIoT.BIZ.Services
{
    public class RoleService : IRoleService
    {
        private readonly IUnitOfWork _unitOfWork;
        public string Error => _unitOfWork.RoleRepository.Error;
        public RoleService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<IEnumerable<Role>> GetAll()
        {
            try
            {
                return _unitOfWork.RoleRepository.GetAll();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Task<Role> Get(int id)
        {
            try
            {
                return _unitOfWork.RoleRepository.Get(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Task<int> Create(Role entity)
        {
            try
            {
                return _unitOfWork.RoleRepository.Create(entity);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public Task<int> Update(int id, Role entity)
        {
            try
            {
                return _unitOfWork.RoleRepository.Update(id, entity);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public Task<int> Delete(int id)
        {
            try
            {
                return _unitOfWork.RoleRepository.Delete(id);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public Task<IEnumerable<Role>> Query(string sql)
        {
            throw new NotImplementedException();
        }
    }
}
