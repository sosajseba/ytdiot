﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Interfaces;

namespace YTDIoT.BIZ.Services
{
    public abstract class GenericService<T> : IGenericService<T> where T : BaseEntity
    {
        private readonly IGenericRepository<T> _repository;

        public string Error => _repository.Error;

        public GenericService(IGenericRepository<T> repository)
        {
            _repository = repository;
        }

        public Task<T> Get(int id)
        {
            try
            {
                return _repository.Get(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Task<int> Create(T entity)
        {
            try
            {
                return _repository.Create(entity);
            }
            catch (Exception)
            {
                throw;
            }
            
        }
        public Task<int> Update(int id, T entity)
        {
            try
            {
                return _repository.Update(id, entity);
            }
            catch (Exception)
            {
                throw;
            }
            
        }
        public Task<int> Delete(int id)
        {
            try
            {
                return _repository.Delete(id);
            }
            catch (Exception)
            {
                throw;
            }
            
        }
        public Task<IEnumerable<T>> Query(string sql)
        {
            throw new NotImplementedException();
        }
    }
}
