﻿using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using YTDIoT.BIZ.Services;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Interfaces;
using YTDIoT.COMMON.Validators;
using YTDIoT.DAL.Repositories;

namespace YTDIoT.BIZ
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            // UnitOfWork
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            // Validators
            services.AddTransient<IValidator<User>, UserValidator>();
            services.AddTransient<IValidator<Role>, RoleValidator>();
            services.AddTransient<IValidator<Data>, DataValidator>();
            services.AddTransient<IValidator<Device>, DeviceValidator>();
            services.AddTransient<IValidator<Channel>, ChannelValidator>();
            // Repositories
            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IChannelRepository, ChannelRepository>();
            services.AddTransient<IDataRepository, DataRepository>();
            services.AddTransient<IDeviceRepository, DeviceRepository>();
            services.AddTransient<IRoleRepository, RoleRepository>();
            // Services
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IChannelService, ChannelService>();
            services.AddTransient<IDataService, DataService>();
            services.AddTransient<IDeviceService, DeviceService>();
            services.AddTransient<IRoleService, RoleService>();
            services.AddTransient<IDataAccess, DataAccess>();
            return services;
        }
    }
}
