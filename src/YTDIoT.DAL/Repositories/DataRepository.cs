﻿using FluentValidation;
using Microsoft.Extensions.Configuration;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Interfaces;

namespace YTDIoT.DAL.Repositories
{
    public class DataRepository : GenericRepository<Data>, IDataRepository
    {
        public DataRepository(IValidator<Data> validator, IConfiguration configuration, IDataAccess dataAccess) : base(validator, configuration, dataAccess)
        {
        }
    }
}
