﻿using FluentValidation;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Interfaces;

namespace YTDIoT.DAL.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        
        public string Error { get; set; }
        private readonly IValidator<T> _validator;
        private readonly IConfiguration _configuration;
        private readonly IDataAccess _dataAccess;
        private readonly string connectionString;

        public GenericRepository(IValidator<T> validator, IConfiguration configuration, IDataAccess dataAccess)
        {
            _validator = validator;
            _configuration = configuration;
            _dataAccess = dataAccess;
            connectionString = _configuration.GetConnectionString("Test");
        }
        public async Task<T> Get(int id)
        {
            try
            {
                List<T> entities;
                string sql = "select * from " + typeof(T).Name + " where Id = " + id;

                entities = await _dataAccess.LoadData<T, dynamic>(sql, new { Id = id }, connectionString);
                Error = entities != null ? "" : "No result";
                return entities.FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }

        }
        public async Task<IEnumerable<T>> GetAll()
        {
            try
            {
                IEnumerable<T> entities;
                string sql = "select * from " + typeof(T).Name;

                entities = await _dataAccess.LoadData<T, dynamic>(sql, new { }, connectionString);
                Error = entities != null ? "" : "No result";
                return entities;

            }
            catch (Exception)
            {
                throw;
            }

        }
        public async Task<int> Create(T entity)
        {
            try
            {
                entity.Date = DateTime.Now;

                var validationResult = _validator.Validate(entity);

                if (validationResult.IsValid)
                {
                    string sql = QueryGenerator<T>.GenerateInsertQuery(typeof(T).Name);
                    int rows = await _dataAccess.SaveData(sql, entity, connectionString);
                    Error = "";
                    return rows;
                }
                else
                {
                    Error = "Validation errors: ";
                    foreach (var item in validationResult.Errors)
                    {
                        Error += item.ErrorMessage + "; ";
                    }
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                throw;
            }

        }
        public async Task<int> Update(int id, T entity)
        {
            try
            {
                entity.Id = id;

                entity.Date = DateTime.Now;

                var validationResult = _validator.Validate(entity);

                if (validationResult.IsValid)
                {
                    string sql = QueryGenerator<T>.GenerateUpdateQuery(typeof(T).Name);
                    var rows = await _dataAccess.SaveData(sql, entity, connectionString);
                    if (rows == 0)
                    {
                        Error = "There was an error while updating User table; ";
                        throw new Exception();
                    }
                    return rows;
                }
                else
                {
                    Error = "Validation errors: ";
                    foreach (var item in validationResult.Errors)
                    {
                        Error += item.ErrorMessage + "; ";
                    }
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<int> Delete(int id)
        {
            try
            {
                string sql = "delete from " + typeof(T).Name + " where Id = " + id;
                var rows = await _dataAccess.SaveData(sql, new { Id = id }, connectionString);
                if (rows == 0)
                {
                    Error = "There was an error while deleting " + typeof(T).Name;
                    throw new Exception();
                }
                return rows;
            }
            catch (Exception)
            {
                throw;
            }

        }
        public async Task<IEnumerable<T>> Query(string sql)
        {
            try
            {
                IEnumerable<T> entities;
                entities = await _dataAccess.LoadData<T, dynamic>(sql, new { }, connectionString);
                Error = entities != null ? "" : "No result";
                return entities;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
