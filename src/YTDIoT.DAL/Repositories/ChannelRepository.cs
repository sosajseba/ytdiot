﻿using FluentValidation;
using Microsoft.Extensions.Configuration;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Interfaces;

namespace YTDIoT.DAL.Repositories
{
    public class ChannelRepository : GenericRepository<Channel>, IChannelRepository
    {
        public ChannelRepository(IValidator<Channel> validator, IConfiguration configuration, IDataAccess dataAccess) : base(validator, configuration, dataAccess)
        {
        }
    }
}
