﻿using FluentValidation;
using Microsoft.Extensions.Configuration;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Interfaces;

namespace YTDIoT.DAL.Repositories
{
    public class DeviceRepository : GenericRepository<Device>, IDeviceRepository
    {
        public DeviceRepository(IValidator<Device> validator, IConfiguration configuration, IDataAccess dataAccess) : base(validator, configuration, dataAccess)
        {
        }
    }
}
