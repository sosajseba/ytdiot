﻿using YTDIoT.COMMON.Interfaces;

namespace YTDIoT.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(IUserRepository userRepository, IRoleRepository roleRepository, IDataRepository dataRepository,
            IDeviceRepository deviceRepository, IChannelRepository channelRepository)
        {
            RoleRepository = roleRepository;
            UserRepository = userRepository;
            DataRepository = dataRepository;
            DeviceRepository = deviceRepository;
            ChannelRepository = channelRepository;
        }
        public IUserRepository UserRepository { get; }
        public IRoleRepository RoleRepository { get; }
        public IDataRepository DataRepository { get; }
        public IDeviceRepository DeviceRepository { get; }
        public IChannelRepository ChannelRepository { get; }
    }
}
