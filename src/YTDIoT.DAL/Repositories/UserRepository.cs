﻿using FluentValidation;
using Microsoft.Extensions.Configuration;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Interfaces;

namespace YTDIoT.DAL.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(IValidator<User> validator, IConfiguration configuration, IDataAccess dataAccess) : base(validator, configuration, dataAccess)
        {
        }
    }
}
