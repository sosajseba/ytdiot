﻿using FluentValidation;
using Microsoft.Extensions.Configuration;
using YTDIoT.COMMON.Entities;
using YTDIoT.COMMON.Interfaces;

namespace YTDIoT.DAL.Repositories
{
    public class RoleRepository : GenericRepository<Role>, IRoleRepository
    {
        public RoleRepository(IValidator<Role> validator, IConfiguration configuration, IDataAccess dataAccess) : base(validator, configuration, dataAccess)
        {
        }
    }
}
